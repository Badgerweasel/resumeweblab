var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'Select * From account'

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(account_id, callback)
{
    var query = 'SELECT a.first_name, a.last_name, a.email, s.school_name, sk.skill_name, c.company_name FROM account a ' +
        'Left Join account_school acs ON a.account_id = acs.account_id ' +
        'Left Join school s ON acs.school_id = s.school_id ' +
        'Left Join account_skill ask ON a.account_id = ask.account_id ' +
        'Left Join skill sk ON ask.skill_id = sk.skill_id ' +
        'Left Join account_company ac ON a.account_id = ac.account_id ' +
        'Left Join company c ON ac.company_id = c.company_id ' +
        'Where a.account_id = ?;';

    var queryData = [account_id];
    console.log(query);
    connection.query(query, queryData, function(err, result){
        console.log("in account get by id result is: " + JSON.stringify(result));
        callback(err, result);
    });
}

exports.insert = function(params, callback)
{
    var query = 'INSERT INTO account (first_name, last_name, email) VALUES (?);';
    var queryData = []
    queryData.push([params.first_name, params.last_name, params.email]);
    connection.query(query, queryData, function(err, result)
    {
        var account_id = result.insertId;

        var query = 'INSERT INTO account_company (account_id, company_id) VALUES ?';

        var companyAccountData = [];
        if (params.company_id.constructor === Array) {
            for (var i = 0; i < params.company_id.length; i++) {
                companyAccountData.push([account_id, params.company_id[i]]);
            }
        }
        else {
            companyAccountData.push([account_id, params.company_id]);
        }
        connection.query(query, [companyAccountData], function (err, result) {
           var query = 'INSERT INTO account_school (account_id, school_id) VALUES ?';
           var schoolAccountData = [];
            if (params.school_id.constructor === Array) {
                for (var i = 0; i < params.school_id.length; i++) {
                    schoolAccountData.push([account_id, params.school_id[i]]);
                }
            }
            else {
                schoolAccountData.push([account_id, params.school_id]);
            }
            console.log('schoolAccountData: ' + schoolAccountData);
            connection.query(query, [schoolAccountData], function (err, result) {
                var query = 'INSERT INTO account_skill (account_id, skill_id) VALUES ?';
                var skillAccountData = [];
                if (params.skill_id.constructor === Array) {
                    for (var i = 0; i < params.skill_id.length; i++) {
                        skillAccountData.push([account_id, params.skill_id[i]]);
                    }
                }
                else {
                    skillAccountData.push([account_id, params.skill_id]);
                }
                connection.query(query, [skillAccountData], function (err, result) {
                    callback(err, result);
                })
            })
        });

    });
}

exports.delete = function(account_id, callback) {
    var query = 'DELETE FROM account_company WHERE account_id = ?';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        console.log(result);
        var query = 'DELETE FROM account_school WHERE account_id = ?';

        connection.query(query, queryData, function(err, result) {
            console.log(result);
            var query = 'DELETE FROM account_skill WHERE account_id = ?';

            connection.query(query, queryData, function(err, result) {
                console.log(result);
                var query = 'DELETE FROM account WHERE account_id = ?';

                connection.query(query, queryData, function(err, result) {
                    console.log(result);
                    callback(err, result);
                });
            });
        });

    });

};

exports.edit = function(account_id, callback) {
    console.log('in edit account id is: ' + account_id);
    var query = 'SELECT a.account_id, a.first_name, a.last_name, a.email, s.school_name, s.school_id, sk.skill_name, sk.skill_id, c.company_name, c.company_id FROM account a ' +
        'Left Join account_school acs ON a.account_id = acs.account_id ' +
        'Left Join school s ON acs.school_id = s.school_id ' +
        'Left Join account_skill ask ON a.account_id = ask.account_id ' +
        'Left Join skill sk ON ask.skill_id = sk.skill_id ' +
        'Left Join account_company ac ON a.account_id = ac.account_id ' +
        'Left Join company c ON ac.company_id = c.company_id ' +
        'Where a.account_id = ?;';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
}

exports.update = function(params, callback) {
    console.log('in address update params is: ' + JSON.stringify(params));
    var query = 'UPDATE account SET first_name = ?, last_name = ?, email = ?  WHERE account_id = ?';
    var queryData = [params.first_name, params.last_name, params.email, params.account_id];
    console.log('queryData: ' + queryData);

    connection.query(query, queryData, function(err, result) {
        console.log('account update: ' + JSON.stringify(result));
        callback(err, result);
    });
}
