var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'Select r.resume_name, r.resume_id, a.first_name, a.last_name, a.account_id From resume r ' +
        'Left Join account a ON r.account_id = a.account_id ' +
        'Order By a.first_name;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(resume_id, callback)
{
    var query = 'SELECT a.first_name, a.last_name, a.email, s.school_name, sk.skill_name, c.company_name FROM account a ' +
        'Left Join account_school acs ON a.account_id = acs.account_id ' +
        'Left Join school s ON acs.school_id = s.school_id ' +
        'Left Join account_skill ask ON a.account_id = ask.account_id ' +
        'Left Join skill sk ON ask.skill_id = sk.skill_id ' +
        'Left Join account_company ac ON a.account_id = ac.account_id ' +
        'Left Join company c ON ac.company_id = c.company_id ' +
        'Where a.account_id = ?;';

    var queryData = [resume_id];
    console.log(query);
    connection.query(query, queryData, function(err, result){
        console.log("in account get by id result is: " + JSON.stringify(result));
        callback(err, result);
    });
}

exports.getSchools = function(account_id, callback)
{
    var query = 'Select s.school_name, s.school_id From account_school acs ' +
        'Left Join school s ON acs.school_id = s.school_id ' +
        'Where acs.account_id = ?;';
    var queryData = [account_id];
    connection.query(query, queryData, function (err, result) {
        callback(err,result);
    })
}

exports.getCompanies = function(account_id, callback)
{
    var query = 'Select c.company_id, c.company_name From account_company ac ' +
        'Left Join company c ON ac.company_id = c.company_id ' +
        'Where ac.account_id = ?'
    var queryData = [account_id];
    connection.query(query, queryData, function (err, result) {
        callback(err, result);
    });
}

exports.getSkills = function (account_id, callback)
{
    var query = 'Select s.skill_id, s.skill_name From account_skill acs ' +
        'Left Join skill s ON acs.skill_id = s.skill_id ' +
        'Where acs.account_id = ?'
    var queryData = [account_id];
    connection.query(query, queryData, function (err, result) {
        callback(err, result);
    });
}

exports.insert = function(params, callback)
{
    var query = 'INSERT INTO resume (account_id, resume_name) VALUES (?);';
    var queryData = []
    queryData.push([params.account_id, params.resume_name]);
    connection.query(query, queryData, function(err, result) {
        var resume_id = result.insertId;

        var query = 'INSERT INTO resume_company (resume_id, company_id) VALUES ?';

        var companyResumeData = [];
        if (params.company_id != null)
        {
            if (params.company_id.constructor === Array) {
                for (var i = 0; i < params.company_id.length; i++) {
                    companyResumeData.push([resume_id, params.company_id[i]]);
                }
            }
            else {
                companyResumeData.push([resume_id, params.company_id]);
            }
        }
        connection.query(query, [companyResumeData], function (err, result) {
            var query = 'INSERT INTO resume_school (resume_id, school_id) VALUES ?';
            var schoolResumeData = [];
            if(params.school_id != null)
            {
                if (params.school_id.constructor === Array) {
                    for (var i = 0; i < params.school_id.length; i++) {
                        schoolResumeData.push([resume_id, params.school_id[i]]);
                    }
                }
                else {
                    schoolResumeData.push([resume_id, params.school_id]);
                }
            }
            console.log('schoolAccountData: ' + schoolResumeData);
            connection.query(query, [schoolResumeData], function (err, result) {
                var query = 'INSERT INTO resume_skill (resume_id, skill_id) VALUES ?';
                var skillResumeData = [];
                if(params.skill_id != null)
                {
                    if (params.skill_id.constructor === Array) {
                        for (var i = 0; i < params.skill_id.length; i++) {
                            skillResumeData.push([resume_id, params.skill_id[i]]);
                        }
                    }
                    else {
                        skillResumeData.push([resume_id, params.skill_id]);
                    }
                }
                connection.query(query, [skillResumeData], function (err, result) {
                    callback(err, result);
                })
            })
        });

    });
}

exports.delete = function(resume_id, callback) {
    var query = 'DELETE FROM resume_company WHERE resume_id = ?';
    var queryData = [resume_id];

    connection.query(query, queryData, function(err, result) {
        console.log(result);
        var query = 'DELETE FROM resume_school WHERE resume_id = ?';

        connection.query(query, queryData, function(err, result) {
            console.log(result);
            var query = 'DELETE FROM resume_skill WHERE resume_id = ?';

            connection.query(query, queryData, function(err, result) {
                console.log(result);
                var query = 'DELETE FROM resume WHERE resume_id = ?';

                connection.query(query, queryData, function(err, result) {
                    console.log(result);
                    callback(err, result);
                });
            });
        });

    });

};

exports.edit = function(resume_id, account_id, callback) {
    console.log('in edit account id is: ' + account_id + ' and resume id: ' + resume_id);
    var query = 'Call resume_getInfo(?)'
    var queryData = [];
    queryData.push([resume_id, account_id]);

    connection.query(query, queryData, function(err, result) {
        console.log("result: " + result);
        callback(err, result);
    });
}

exports.update = function(params, callback) {
    var query = 'Update resume SET resume_name = ? Where resume_id = ?;';
    var queryData = [params.resume_name, params.resume_id];
    console.log('Update: ' + params.resume_name + ' ' + params.resume_id);

    connection.query(query, queryData, function(err, result) {

        var resume_id = params.resume_id;
        console.log('resume_id: ' + resume_id);
        deleteResumeCompany(resume_id, function (err, result) {
            var query = 'INSERT INTO resume_company (resume_id, company_id) VALUES ?';

            var companyResumeData = [];
            if (params.company_id != null)
            {
                if (params.company_id.constructor === Array) {
                    for (var i = 0; i < params.company_id.length; i++) {
                        companyResumeData.push([resume_id, params.company_id[i]]);
                    }
                }
                else {
                    companyResumeData.push([resume_id, params.company_id]);
                }
            }
            connection.query(query, [companyResumeData], function (err, result) {

                deleteResumeSchool(resume_id, function (err, result)
                {
                    var query = 'INSERT INTO resume_school (resume_id, school_id) VALUES ?';
                    var schoolResumeData = [];
                    if(params.school_id != null)
                    {
                        if (params.school_id.constructor === Array) {
                            for (var i = 0; i < params.school_id.length; i++) {
                                schoolResumeData.push([resume_id, params.school_id[i]]);
                            }
                        }
                        else {
                            schoolResumeData.push([resume_id, params.school_id]);
                        }
                    }
                    console.log('schoolAccountData: ' + schoolResumeData);
                    connection.query(query, [schoolResumeData], function (err, result) {

                        deleteResumeSkill(resume_id, function (err, result)
                        {
                            var query = 'INSERT INTO resume_skill (resume_id, skill_id) VALUES ?';
                            var skillResumeData = [];
                            if(params.skill_id != null)
                            {
                                if (params.skill_id.constructor === Array) {
                                    for (var i = 0; i < params.skill_id.length; i++) {
                                        skillResumeData.push([resume_id, params.skill_id[i]]);
                                    }
                                }
                                else {
                                    skillResumeData.push([resume_id, params.skill_id]);
                                }
                            }
                            connection.query(query, [skillResumeData], function (err, result) {

                                callback(err, result);
                            });
                        });
                    });
                });
            });
        });
    });
}

var deleteResumeCompany = function (resume_id, callback)
{
    var query = 'Delete From resume_company Where resume_id = ?'

    connection.query(query, [resume_id], function (err, result)
    {
        if(err)
        {
            console.log('in deleteResumeCompany: ' + err)
        }
        else
        {
            callback(err, result);
        }
    });
}

var deleteResumeSchool = function (resume_id, callback) {
    var query = 'Delete From resume_school Where resume_id = ?'

    connection.query(query, [resume_id], function (err, result) {
        if(err)
        {
            console.log('in deleteResumeSchool: ' + err)
        }
        else
        {
            callback(err, result);
        }
    });
}

var deleteResumeSkill = function (resume_id, callback) {
    var query = 'Delete From resume_skill Where resume_id = ?'

    connection.query(query, [resume_id], function (err, result) {
        if(err)
        {
            console.log('in deleteResumeSkill: ' + err)
        }
        else
        {
            callback(err, result);
        }
    });
}