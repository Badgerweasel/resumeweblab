var express = require('express');
var router = express.Router();
var account_dal = require('../model/account_dal');
var school_dal = require('../model/school_dal');
var company_dal = require('../model/company_dal');
var skill_dal = require('../model/skill_dal');

//view all addresses
router.get('/all', function (req, res) {
    account_dal.getAll(function (err, result) {
        if(err)
        {
            res.send(err);
        }
        else
        {
            res.render('account/accountViewAll', {'result':result});
        }
    });
});

//view the address for the given id
router.get('/', function(req, res) {
    if(req.query.account_id == null)
    {
        res.send('account_id is null');
    }
    else
    {
        account_dal.getById(req.query.account_id, function(err, result){
            if(err)
            {
                res.send(err);
            }
            else
            {
                res.render('account/accountViewById', {'result': result});
            }
        });
    }
});

//return the add a new address form
router.get('/add', function(req, res){

    company_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            var res1 = result;
            school_dal.getAll(function(err,result) {
                if(err){
                    res.send(err);
                }
                else
                {
                    var res2 = result;
                    skill_dal.getAll(function(err, result){
                        if(err){
                            res.send(err);
                        }
                        else
                        {
                            var res3 = result;
                            var final = {'company': res1 , 'school': res2, 'skill': res3};
                            console.log('in account add final is: ' + JSON.stringify(final));
                            res.render('account/accountAdd', final);
                        }
                    });
                }
            });
        }
    });
});

router.get('/insert', function(req, res){
    if(req.query.first_name == null)
    {
        res.send('first name must be provided.');
    }
    else if(req.query.last_name == null)
    {
        res.send('last name must be provided.');
    }
    else if(req.query.email == null)
    {
        res.send('email must be provided.');
    }
    else
    {
        account_dal.insert(req.query, function(err,result) {
            if (err)
            {
                console.log(err)
                res.send(err);
            }
            else
            {
                res.redirect(302, '/account/all');
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.account_id == null) {
        res.send('A skill id is required');
    }
    else {
        account_dal.edit(req.query.account_id, function(err, result){
            console.log('in account edit result is: ' + JSON.stringify(result));
            res.render('account/accountUpdate', {account: result});
        });
    }
});

router.get('/update', function(req, res) {
    account_dal.update(req.query, function(err, result){
        if(err)
        {
            console.log(err)
            res.send(err);
        }
        else
        {
            res.redirect(302, '/account/all');
        }

    });
});

router.get('/delete', function(req, res) {
    if(req.query.account_id == null) {
        res.send('account_id is null');
    }
    else {
        account_dal.delete(req.query.account_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                res.redirect(302, '/account/all');
            }
        });
    }
});

module.exports = router;