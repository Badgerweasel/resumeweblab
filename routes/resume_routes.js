var express = require('express');
var router = express.Router();
var resume_dal = require('../model/resume_dal');
var account_dal = require('../model/account_dal');
var school_dal = require('../model/school_dal');
var company_dal = require('../model/company_dal');
var skill_dal = require('../model/skill_dal');


router.get('/all', function (req, res) {
    resume_dal.getAll(function (err, result) {
        if(err)
        {
            res.send(err);
        }
        else
        {
            console.log('resume_name: ' + JSON.stringify(req.query));
            res.render('resume/resumeViewAll', {'result':result, was_successful:req.query.was_successful, resume_name:req.query.resume_name});
        }
    });
});

//view the address for the given id
router.get('/', function(req, res) {
    if(req.query.resume_id == null)
    {
        res.send('resume_id is null');
    }
    else
    {
        resume_dal.getById(req.query.resume_id, function(err, result){
            if(err)
            {
                res.send(err);
            }
            else
            {
                res.render('resume/resumeViewById', {'result': result});
            }
        });
    }
});

router.get('/add/selectuser', function (req, res) {
    account_dal.getAll(function (err, result) {
        if(err)
        {
            res.send(err);
        }
        else
        {
            console.log("resume selectUser: " + JSON.stringify(result));
            res.render('resume/resumeSelectUser', {"result": result});
        }
    });
});

//return the add a new address form
router.get('/add', function(req, res)
{
    if(req.query.account_id == null)
    {
        res.send('account_id must be provided');
    }
    else
    {
        resume_dal.getSchools(req.query.account_id, function (err, resultSchools) {

            resume_dal.getCompanies(req.query.account_id, function (err, resultCompanies) {

                resume_dal.getSkills(req.query.account_id, function (err, resultSkills) {
                    res.render('resume/resumeAdd', {'account_id': req.query.account_id, 'school': resultSchools, 'company': resultCompanies, 'skill': resultSkills});
                });
            });
        });
    }
});

router.post('/insert', function(req, res){
    if(req.body.resume_name == null)
    {
        res.send('resume name must be provided.');
    }
    else
    {
        resume_dal.insert(req.body, function(err,result) {
            if (err)
            {
                console.log(err)
                res.send(err);
            }
            else
            {
                res.redirect(302, 'resume/all');
            }
        });
    }
});

router.post('/edit', function(req, res){
    if(req.body.resume_id == null) {
        res.send('A skill id is required');
    }
    else {
        console.log('req: ' + JSON.stringify(req.body));
        resume_dal.edit(req.body.resume_id, req.body.account_id, function(err, result){
            console.log('in account edit result is: ' + JSON.stringify(result[0]));
            res.render('resume/resumeUpdate', {resume: result[0][0], skill: result[1], company: result[2], school: result[3]}, was_successful = false);
        });
    }
});

router.post('/update', function(req, res) {
    resume_dal.update(req.body, function(err, result){
/*
        var tempJSON = {'resume':{'account_id':req.body.account_id, 'resume_id':req.body.resume_id, 'resume_name':req.body.resume_name},
            'skill':{'skill_id':req.body.skill_id}, 'company':{'company_id':req.body.company_id},
            'school':{'school_id':req.body.school_id}, 'was_successful':true}
        res.render('resume/resumeUpdate', tempJSON);*/

        res.redirect('/resume/all/?was_successful=true&resume_name=' +  req.body.resume_name );

    });
});

router.get('/delete', function(req, res) {
    if(req.query.resume_id == null) {
        res.send('resume_id is null');
    }
    else {
        resume_dal.delete(req.query.resume_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                res.redirect(302, '/resume/all');
            }
        });
    }
});

module.exports = router;