var express = require('express');
var router = express.Router();
var skill_dal = require('../model/skill_dal');

//view all addresses
router.get('/all', function (req, res) {
    skill_dal.getAll(function (err, result) {
        if(err)
        {
            res.send(err);
        }
        else
        {
            res.render('skill/skillViewAll', {'result':result});
        }
    });
});

//view the address for the given id
router.get('/', function(req, res) {
    if(req.query.skill_id == null)
    {
        res.send('skill_id is null');
    }
    else
    {
        skill_dal.getById(req.query.skill_id, function(err, result){
            if(err)
            {
                res.send(err);
            }
            else
            {
                res.render('skill/skillViewById', {'result': result});
            }
        });
    }
});

//return the add a new address form
router.get('/add', function(req, res){
    skill_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('skill/skillAdd', {'skill': result});
        }
    });
});

router.get('/insert', function(req, res){
    if(req.query.skill_name == null)
    {
        res.send('name must be provided.');
    }
    else if(req.query.description == null)
    {
        res.send('description must be provided');
    }
    else
    {
        skill_dal.insert(req.query, function(err,result) {
            if (err)
            {
                console.log(err)
                res.send(err);
            }
            else
            {
                res.redirect(302, '/skill/all');
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.skill_id == null) {
        res.send('A skill id is required');
    }
    else {
        skill_dal.edit(req.query.skill_id, function(err, result){
            console.log('in skill edit result is: ' + JSON.stringify(result));
            res.render('skill/skillUpdate', {skill: result[0]});
        });
    }
});

router.get('/update', function(req, res) {
    skill_dal.update(req.query, function(err, result){
        if(err)
        {
            console.log(err)
            res.send(err);
        }
        else
        {
            res.redirect(302, '/skill/all');
        }

    });
});

router.get('/delete', function(req, res) {
    if(req.query.skill_id == null) {
        res.send('skill_id is null');
    }
    else {
        skill_dal.delete(req.query.skill_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                res.redirect(302, '/skill/all');
            }
        });
    }
});

module.exports = router;